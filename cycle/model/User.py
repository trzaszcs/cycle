'''
Created on 28-12-2012

@author: trzaszcs
'''
from google.appengine.ext import db

class User(db.Model):
    email = db.StringProperty(required=True)
    calendarId = db.StringProperty(required=True)
