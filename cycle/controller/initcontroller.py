from google.appengine.api import users
import webapp2
from google.appengine.ext.webapp import template
from cycle.utils import utils
from cycle.utils import oauth
import logging

decorator=oauth.get_decorator()

class Controller(webapp2.RequestHandler):
    
    @decorator.oauth_aware
    def get(self):
        user = users.get_current_user()
        
        if user:
            
            if decorator.has_credentials():
                logging.info("checking wheather user is new !")
                utils.init_new_user(decorator.http())
                self.redirect("main")
            else:
                template_values = {
                    "user":user.email(),
                    "url":decorator.authorize_url()
                }
                path = utils.get_template_path('init.html')
                self.response.out.write(template.render(path, template_values))
        else:
            self.redirect(users.create_login_url(self.request.uri))
    
    
    