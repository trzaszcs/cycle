from google.appengine.api import users
from google.appengine.ext import webapp
from cycle.model.user import User
import cycle.calendar.service
import datetime

class Controller(webapp.RequestHandler):
    
    
    def get(self):
        user = users.get_current_user()

        if user:
            
            #extract credentials
            
            credentials=cycle.calendar.service.extract_credentials(self.request.get("code"))
            
            #save credentials 
            self._save_user(user, credentials)
            
            self.redirect("main")
        else:
            self.redirect(users.create_login_url(self.request.uri))
    
    def _save_user(self,u,credentials):
        user=User(email=u.email(),credentials=credentials,creation_date=datetime.datetime.now().date())
        user.save()
    