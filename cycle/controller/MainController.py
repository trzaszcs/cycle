from google.appengine.api import users
import webapp2
from google.appengine.ext.webapp import template
from cycle.calendar.service import CalendarService
from cycle.utils import utils
import logging
import json
from cycle.utils import oauth

decorator = oauth.get_decorator()

class Controller(webapp2.RequestHandler):
    
    @decorator.oauth_required
    def get(self):
        user = users.get_current_user()

        if user:
            if True:
                
                http = decorator.http()
                service = CalendarService(http)
                
                logging.info("dbuser found " + user.email())
                
                template_values = {
                    "calendars":service._get_calendars()
                }
                path = utils.get_template_path('main.html')
                self.response.out.write(template.render(path, template_values))
            else:
                self.redirect("/");
        else:
            self.redirect(users.create_login_url(self.request.uri))
    

class EventsController(webapp2.RequestHandler):            
            
    @decorator.oauth_required
    def get(self):
        date_str = self.request.get("date")
        
        http = decorator.http()
        service = CalendarService(http)
        
        calendarId = utils.get_calendar_id()
        date_from, date_to = utils.get_date_ranges(date_str)
        
        events = service.get_events(calendarId, date_from, date_to)
        
        resp = webapp2.Response()
        
        resp.write(json.dumps(events))
        
        return resp
    
    @decorator.oauth_required
    def post(self):
        
        date_str = self.request.get("date")
        type=self.request.get("type")
        
        http = decorator.http()
        CalendarService(http).create_event(date_str, type);
        
        self.response.write("OK")


class DeleteEventController(webapp2.RequestHandler):            
            
    @decorator.oauth_required
    def get(self):
        
        eventId=self.request.get("id")
        
        http = decorator.http()
        CalendarService(http).delete_event(eventId)
        self.response.write("OK")
        

