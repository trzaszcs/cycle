'''
Created on 28-12-2012

@author: trzaszcs
'''
import gdata.gauth
import cycle.config.config


def get_token():
    token = gdata.gauth.OAuth2Token(client_id=cycle.config.config.CLIENT_ID, client_secret=cycle.config.config.CLIENT_SECRET, scope=' '.join(cycle.config.config.SCOPES),user_agent=cycle.config.config.USER_AGENT)
    token.generate_authorize_url(redirect_uri=cycle.config.config.REDIRECT_URI)
    return token

def get_url():
    token = gdata.gauth.OAuth2Token(client_id=cycle.config.config.CLIENT_ID, client_secret=cycle.config.config.CLIENT_SECRET, scope=' '.join(cycle.config.config.SCOPES),user_agent=cycle.config.config.USER_AGENT)
    return token.generate_authorize_url(redirect_uri=cycle.config.config.REDIRECT_URI)

def auth_token(code):
    token=get_token()
    return token.get_access_token(code)