'''
Created on 05-01-2013

@author: Czacha
'''
import os
import datetime

from cycle.model.user import User
import cycle.service.userservice
from cycle.calendar.service import CalendarService
from google.appengine.api import users

def get_template_path(view_name):
    template_sufix='../../webapp/template/'+view_name
    return os.path.join(os.path.dirname(__file__), template_sufix)

def get_date(date_str):
    d=datetime.datetime.strptime(date_str,"%d-%m-%y")
    return d.isoformat()+"+0000";

def get_date_ranges(date_str):
    d=datetime.datetime.strptime(date_str,"%d-%m-%y")
    day=d.day
    
    date_from=d-datetime.timedelta(days=(day-1))
    date_to=d+datetime.timedelta(days=(31-day))
    
    return date_from.isoformat()+"+0000",date_to.isoformat()+"+0000"

def init_new_user(http):
    
    user = users.get_current_user()
    dbuser=cycle.service.userservice.get_user(user.email())
    service=CalendarService(http)
    calendar_id=service.get_cycle_calendar_id()
    
    if not calendar_id:
        calendar_id=service.init_calendar()
    
    if dbuser:
        dbuser.calendarId=calendar_id
        
    else:
        dbuser=User(email=user.email(),calendarId=calendar_id)
    
    dbuser.put()
    
    return dbuser

def get_calendar_id():
    user = users.get_current_user()
    dbuser=cycle.service.userservice.get_user(user.email())
    return dbuser.calendarId
    
    
    