'''
Created on 06-01-2013

@author: Czacha
'''
import os
from oauth2client.appengine import OAuth2DecoratorFromClientSecrets
from apiclient.discovery import build

CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), '../config/client_secrets.json')

def get_decorator():
    return OAuth2DecoratorFromClientSecrets(CLIENT_SECRETS, 'https://www.googleapis.com/auth/calendar')

def get_service():
    return build('calendar', 'v3')