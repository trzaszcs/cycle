'''
Created on 04-01-2013

@author: Czacha
'''
import os
from oauth2client.appengine import oauth2decorator_from_clientsecrets
from oauth2client.client import flow_from_clientsecrets
from cycle.model import user
import httplib2
from apiclient.discovery import build

CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), '../config/client_secrets.json')
APP_CALENDAR_NAME = "CYCLE";

def get_auth_decorator():
    decorator = oauth2decorator_from_clientsecrets(CLIENT_SECRETS, scope='https://www.googleapis.com/auth/calendar')
    return decorator;

def get_flow():
    return flow_from_clientsecrets(CLIENT_SECRETS, scope='https://www.googleapis.com/auth/calendar', redirect_uri='http://pipajus-hrd.appspot.com/oauth2callback')


def get_auth_url():
    return get_flow().step1_get_authorize_url();

def extract_credentials(code):
    return get_flow().step2_exchange(code)

def save_credentials(email, credentials):
    user = user.User(email=email, credentials=credentials)
    user.save()
    
def get_credentials(email):
    return user.User.gql("WHERE email=?1", email).get().credentials()

def get_service(credentials):
    http = httplib2.Http()
    http = credentials.authorize(http)
    service = build('calendar', 'v3', http=http)
    return service
    
def get_calendars(credentials):
    service = get_service(credentials)
    calendar_list = service.calendarList().list().execute()
    
    calendars = []
    
    for calendar_list_entry in calendar_list['items']:
        calendars.append(calendar_list_entry["summary"]);

    return calendars
    
    
class CalendarService:
    
    def __init__(self, http):
        self.service = self._get_service(http)
    
    
    def _get_service(self, http):
        service = build('calendar', 'v3', http=http)
        return service
    
    def _get_calendars(self):
        calendar_list = self.service.calendarList().list().execute()
        
        calendars = []
        
        for calendar_list_entry in calendar_list['items']:
            calendars.append(calendar_list_entry["summary"]);
    
        return calendars
    
    def get_cycle_calendar_id(self):
        calendar_list = self.service.calendarList().list().execute()
        
        for calendar_list_entry in calendar_list['items']:
            if calendar_list_entry["summary"] == APP_CALENDAR_NAME:
                return calendar_list_entry["id"]
           
    def get_events(self,calendar_id, date_from, date_to):
        return self.service.events().list(calendarId=calendar_id, timeMin=date_from, timeMax=date_to).execute()
    
    def create_event(self,date,event_type):
        calendar_id=self.get_cycle_calendar_id()
        
        event_entity={"type":event_type};
        
        event={
               'summary': "cycle",
               'description' :str(event_entity),
               'location': 'my body',
               'start':{"date":date},
               'end':{"date":date},
        }
        self.service.events().insert(calendarId=calendar_id, body=event).execute()
        
    def delete_event(self,eventId):
        calendar_id=self.get_cycle_calendar_id()
        self.service.events().delete(calendarId=calendar_id, eventId=eventId).execute()

    def has_cycle_calendar(self):
        calendars = self._get_calendars()
        return calendars.count(APP_CALENDAR_NAME) == 0
    
    def init_calendar(self):
        if self._get_calendars().count(APP_CALENDAR_NAME) :
            return None
        else :
            calendar_entry = {'summary': APP_CALENDAR_NAME}
            calendar_entry=self.service.calendars().insert(body=calendar_entry).execute()
            return calendar_entry["id"]



