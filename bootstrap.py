from cycle.controller import initcontroller
from cycle.controller import maincontroller
import webapp2
from cycle.utils import oauth

decorator=oauth.get_decorator()

app = webapp2.WSGIApplication([('/', initcontroller.Controller),
                               ('/main', maincontroller.Controller),
                               ('/main/events', maincontroller.EventsController),
                               ('/main/deleteEvents', maincontroller.DeleteEventController),
                               (decorator.callback_path,decorator.callback_handler())], 
                              debug=True)