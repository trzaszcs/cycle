'''
Created on 05-01-2013

@author: Czacha
'''
import unittest
import cycle.utils.utils 
import os

class TestUtils(unittest.TestCase):


    def test_get_template_path(self):
        template_path=cycle.utils.utils.get_template_path("init.html")
        self.assertIsNotNone(template_path);
        self.assertTrue(os.path.exists(template_path))
        pass
    
    
    def test_get_time_range(self):
        date_from,date_to=cycle.utils.utils.get_date_ranges("03-01-13")
        self.assertIsNotNone(date_from)
        self.assertIsNotNone(date_to)

if __name__ == '__main__':
    unittest.main()