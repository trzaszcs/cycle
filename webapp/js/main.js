$(document).ready(function() {
	View.callViewFunction();
});


var View={
	main:{
		init:function(){
			CalendarService.getForCurrentMonth();
		}
	},
	init:{
		init:function(){
			
		}
	},
	
	callViewFunction:function(){
		var viewId=$("[data-view-id]");
		
		if(viewId.length==1){
			var viewId=viewId.data("view-id");
			View[viewId].init();
		}
	}
};

var CalendarService={
	getForCurrentMonth:function(){
		
		var d=new Date();
		
		var date=CalendarService.formatDate(d);
		CalendarService.drawCalendar(d);
		
		Server.getEvents(date, function(data){
			$(".calendar_board td[data-date]").each(function(){
				var timeInMillis=$(this).data("date");
				var cellDate=new Date(timeInMillis);
				var formattedDate=CalendarService.getLongDateFormat(cellDate);
				
				for(var c=0;c<data.length;c++){
					if(formattedDate===data[c]["startDate"]){
						$(this).data("id",data[c]["id"]);
						$(this).data("type",data[c]["type"]);
						
						$(this).addClass("period");
					}
				}
				
				
			});
		});
	},
	
	formatDate:function(date){
		var year=date.getYear().toString().substring(1,4);
		
		var month=date.getMonth()+1;
		
		if(month<9){
			month="0"+month
		}
		
		var day=date.getDate();
		
		if(day<9){
			day="0"+day;
		}
		
		return day+"-"+month+"-"+year;
	},
	
	drawCalendar:function(date){
		date.setDate(1);
		
		var firstDay=date.getDay();
		var daysInMonth=new Date(date.getYear(), date.getMonth(), 0).getDate();
		
		// description
		
		$(".month").html(this.getDescription(date, daysInMonth));
		
		
		// draw empty slots
		
		// before
		
		for(var c=1;c<firstDay;c++){
			$(".calendar_board td#"+c).addClass("empty");
		}
		
		// after
		
		if(daysInMonth<31){
			for(var c=daysInMonth;c<=31;c++){
				$(".calendar_board td#"+c).addClass("empty");
			}
		}
	
		
		// draw days
		
		var d=1;
		for(var c=firstDay;c<(daysInMonth+firstDay);c++){
			var dayCell=$(".calendar_board td#"+c);
			dayCell.html(d);
			var date=new Date(date.getFullYear(), date.getMonth(), d);
			dayCell.attr("data-date",date.getTime());
			dayCell.unbind("click");
			
			dayCell.click(function(){
				var cell=$(this);
				var date=new Date(cell.data("date"));
				var dateStr=CalendarService.getLongDateFormat(date);
				
				if(cell.data("id")){
					Server.removeEvent(cell.data("id"),function(data){alert("Event has been removed")});
				}else{
					Server.createEvent(dateStr,"period",function(data){alert("Event has been added")});
				}
			});
			d++;
		}
	},
	
	getLongDateFormat:function(date){
		var dateStr=date.getFullYear()+"-";
		
		var month=date.getMonth()+1;
		
		if(month<10){
			dateStr+="0"+month;
		}else{
			dateStr+=month;
		}
		
		dateStr+="-";
		
		if(date.getDate()<10){
			dateStr+="0"+date.getDate();
		}else{
			dateStr+=date.getDate();
		}
		
		return dateStr;
	},
	
	getDescription:function(date,daysInMonth){
		return 1+"-"+daysInMonth+"."+(date.getMonth()+1+"."+date.getFullYear());
	}
}

var Server={
		getEvents:function(date,callback){
			$.ajax({
				  url: 'main/events',
				  data:{"date":date},
				  success: function(data) {
					  var json=$.parseJSON(data);
					  callback(Server.parseEvents(json.items));
				  }
			});
		},
		
		createEvent:function(date,type,callback){
			$.ajax({
				  url: 'main/events',
				  data:{"date":date,"type":type},
				  type:"POST",
				  success: function(data) {
					  callback(data);
				  }
			});
		},
		
		removeEvent:function(id,callback){
			$.ajax({
				  url: 'main/removeEvent',
				  data:{"id":id},
				  success: function(data) {
					  callback(data);
				  }
			});
		},
		
		parseEvents:function(events){
			var result=[];
			
			if(!(events instanceof Array)){
				var e=[events];
				events=e;
			}
			
			
			for(var c=0;c<events.length;c++){
				var event=events[c];
				
				if(event.summary==="cycle"){
					var startDate=event.start.date;
					var description=event.description;
					var id=event.id;
					result.push({"id":id,"startDate":startDate,"description":description});
				}
			}
			
			
			return result;
		}
};